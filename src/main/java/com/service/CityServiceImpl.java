package com.service;

import com.entity.City;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Random;

@Service
public class CityServiceImpl implements CityService {

    public List<City> findAllCity(){
        return null;
    }

    public City findCityById(Long id){
        return null;
    }

    public Long saveCity(City city){return new Random().nextLong();}

    public Long updateCity(City city){return new Random().nextLong();}

    public Long deleteCity(Long id){return new Random().nextLong();}
}
